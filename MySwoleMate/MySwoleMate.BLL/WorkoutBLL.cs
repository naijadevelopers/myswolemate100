﻿using MySwoleMate.DAL;
using MySwoleMate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySwoleMate.BLL
{
    public class WorkoutBLL
    {
        private WorkoutDAL data;

        public WorkoutBLL(string _connectionString)
        {
            data = new WorkoutDAL(_connectionString);
        }


        //Gets all Trainees in a List of WorkoutViewModel
        public List<WorkoutViewModel> GetAllWorkouts()
        {
            return data.GetWorkoutList();
        }

        //Returns ViewModel of Workout by the Id
        public WorkoutViewModel GetWorkoutById(int id)
        {
            return data.GetWorkOutById(id);
        }

        //Edits the Workout accepting a WorkoutViewModel
        public int EditWorkout(WorkoutViewModel edit)
        {
            return data.EditWorkOut(edit);
        }

        //Adds a new Workout
        public int AddWorkout(WorkoutViewModel add)
        {
            return data.AddWorkoutItem(add);
        }

        //Deletes a Workout by the Id, Delete only needs the id of Workout
        public int DeleteWorkout(int id)
        {
            return data.DeleteWorkout(id);
        }

    }
}
