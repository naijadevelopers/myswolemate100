﻿using MySwoleMate.BLL;
using MySwoleMate.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MySwoleMate
{
    public partial class AssignWorkout : System.Web.UI.Page
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["MySwoleMateConnectionString"].ToString();

        WorkoutBLL workoutBLL = new WorkoutBLL(connectionString);
        TraineeBLL traineeBLL = new TraineeBLL(connectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //Get Connection
            if (!IsPostBack)
            {

                //Bind Name
                var trainee = traineeBLL.GetTraineeById(Convert.ToInt32(Request.QueryString["TraineeID"]));
                TraineeLabel.Text = string.Format("{0} {1}",trainee.FirstName, trainee.LastName);

                List<WorkoutViewModel> workoutList = workoutBLL.GetAllWorkouts();

                WorkoutDropList.DataTextField = "Name";
                WorkoutDropList.DataValueField = "WorkoutID";
                WorkoutDropList.DataSource = workoutList;
                WorkoutDropList.DataBind();

                WorkoutDropList.Items.Insert(0, new ListItem("---Select Workout---", "0"));

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AssignBtn_Click(object sender, EventArgs e)
        {
            
            //Get Workout ID and Name ID
            if ((Convert.ToInt32(Request.QueryString["TraineeID"])) > 0 && Convert.ToInt16(WorkoutDropList.SelectedValue) > 0)
            {
                traineeBLL.AssignWorkoutToTrainee(Convert.ToInt32(Request.QueryString["TraineeID"]), Convert.ToInt16(WorkoutDropList.SelectedValue));

                Response.Redirect("~/Trainees.aspx");
            }
            else
            {
                TraineeLabel.Text = "No Trainee Information to update";
            }
        }
    }
}