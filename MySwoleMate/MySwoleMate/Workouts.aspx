﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MySwoleMate.Master" AutoEventWireup="true" CodeBehind="Workouts.aspx.cs" Inherits="MySwoleMate.Workouts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="first">
        <div class="container">
            <div class="row">
                <div class="col-xs-10">
                    <h1>Workout List</h1>
                </div>
                <div class="col-xs-2">
                    <a href="~/AddWorkout.aspx" runat="server" class="btn btn-success">Add New Workout</a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <asp:GridView ID="WorkoutGridview" runat="server" CssClass="table table-bordered text-left" AutoGenerateColumns="False" 
                        DataKeyNames="WorkoutID" OnRowDeleting="WorkoutList_RowDeleting">
                        <Columns>

                            <asp:BoundField DataField="DisplayExercise1" HeaderText="Exercise 1" />
                            <asp:BoundField DataField="DisplayExercise2" HeaderText="Exercise 2" />
                            <asp:BoundField DataField="DisplayExercise3" HeaderText="Exercise 3" />
                            <asp:BoundField DataField="DisplayExercise4" HeaderText="Exercise 4" />
                            <asp:BoundField DataField="DisplayExercise5" HeaderText="Exercise 5" />
                            <asp:HyperLinkField DataNavigateUrlFields="WorkoutID" DataNavigateUrlFormatString="~/AddWorkout.aspx?WorkoutID={0}" Text="Edit" 
                                ControlStyle-CssClass="btn btn-success btn-xs" ItemStyle-CssClass="text-center" />
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:Button ID="DeleteBtn" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete" CssClass="btn btn-xs btn-default" 
                                        OnClientClick="if(!confirm('Are you sure you wish to delete this workout?')) return false;" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </div>
            </div>
        </div>
    </section>
</asp:Content>
