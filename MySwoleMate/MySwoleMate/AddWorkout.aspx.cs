﻿using MySwoleMate.BLL;
using MySwoleMate.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MySwoleMate
{
    public partial class AddWorkout : System.Web.UI.Page
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["MySwoleMateConnectionString"].ToString();

        WorkoutBLL workoutBLL = new WorkoutBLL(connectionString);

        WorkoutViewModel model = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Only load the values if the request is not a PostBack
            if (!IsPostBack)
            {
                //We pass the WorkoutID by a Query String from the GridView on Workouts.aspx. See how we passed 
                //WorkoutID to this page from Workouts.aspx by looking at HyperLinkField in the GridView control
                //on the Workouts.aspx page for clarification
                if ((Convert.ToInt32(Request.QueryString["WorkoutID"])) > 0)
                {
                    //Update title label display
                    lblWorkout.Text = "Edit Workout";

                    //Get workout from db
                    model = workoutBLL.GetWorkoutById(Convert.ToInt32(Request.QueryString["WorkoutID"]));
                   
                    //populate the data into the form.
                    NameTextBox.Text = model.Name;
                    Exercise1.Text = model.Excercise1;
                    Exercise1Reps.Text = model.Excercise1Reps.ToString();
                    Exercise1Sets.Text = model.Excercise1Sets.ToString();
                    Exercise2.Text = model.Excercise2;
                    Exercise2Reps.Text = model.Excercise2Reps.ToString();
                    Exercise2Sets.Text = model.Excercise2Sets.ToString();
                    Exercise3.Text = model.Excercise1;
                    Exercise3Reps.Text = model.Excercise3Reps.ToString();
                    Exercise3Sets.Text = model.Excercise3Sets.ToString();
                    Exercise4.Text = model.Excercise4;
                    Exercise4Reps.Text = model.Excercise4Reps.ToString();
                    Exercise4Sets.Text = model.Excercise4Sets.ToString();
                    Exercise5.Text = model.Excercise5;
                    Exercise5Reps.Text = model.Excercise5Reps.ToString();
                    Exercise5Sets.Text = model.Excercise5Sets.ToString();
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void WorkoutAddButton_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {
                if ((Convert.ToInt32(Request.QueryString["WorkoutID"])) > 0)
                {
                    model = new WorkoutViewModel();

                    //We will be updating the record
                    model.WorkoutID = Convert.ToInt32(Request.QueryString["WorkoutID"]);
                    model.Name = NameTextBox.Text;
                    model.Excercise1 = Exercise1.Text;
                    model.Excercise1Reps = Convert.ToInt16(Exercise1Reps.Text);
                    model.Excercise1Sets = Convert.ToInt16(Exercise1Sets.Text);
                    model.Excercise2 = Exercise2.Text;
                    model.Excercise2Reps = Convert.ToInt16(Exercise2Reps.Text);
                    model.Excercise2Sets = Convert.ToInt16(Exercise2Sets.Text);
                    model.Excercise3 = Exercise3.Text;
                    model.Excercise3Reps = Convert.ToInt16(Exercise3Reps.Text);
                    model.Excercise3Sets = Convert.ToInt16(Exercise3Sets.Text);
                    model.Excercise4 = Exercise4.Text;
                    model.Excercise4Reps = Convert.ToInt16(Exercise4Reps.Text);
                    model.Excercise4Sets = Convert.ToInt16(Exercise4Sets.Text);
                    model.Excercise5 = Exercise5.Text;
                    model.Excercise5Reps = Convert.ToInt16(Exercise5Reps.Text);
                    model.Excercise5Sets = Convert.ToInt16(Exercise5Sets.Text);

                    workoutBLL.EditWorkout(model);

                    //Return to the Trainees list after adding the trainee to database
                    Response.Redirect("~/Workouts.aspx");
                }
                else
                {
                    //Add new workout
                    model = new WorkoutViewModel();

                    model.Name = NameTextBox.Text;
                    model.Excercise1 = Exercise1.Text;
                    model.Excercise1Reps = Convert.ToInt16(Exercise1Reps.Text);
                    model.Excercise1Sets = Convert.ToInt16(Exercise1Sets.Text);
                    model.Excercise2 = Exercise2.Text;
                    model.Excercise2Reps = Convert.ToInt16(Exercise2Reps.Text);
                    model.Excercise2Sets = Convert.ToInt16(Exercise2Sets.Text);
                    model.Excercise3 = Exercise3.Text;
                    model.Excercise3Reps = Convert.ToInt16(Exercise3Reps.Text);
                    model.Excercise3Sets = Convert.ToInt16(Exercise3Sets.Text);
                    model.Excercise4 = Exercise4.Text;
                    model.Excercise4Reps = Convert.ToInt16(Exercise4Reps.Text);
                    model.Excercise4Sets = Convert.ToInt16(Exercise4Sets.Text);
                    model.Excercise5 = Exercise5.Text;
                    model.Excercise5Reps = Convert.ToInt16(Exercise5Reps.Text);
                    model.Excercise5Sets = Convert.ToInt16(Exercise5Sets.Text);

                    workoutBLL.AddWorkout(model);

                    //Return to the Trainees list after adding the trainee to database
                    Response.Redirect("~/Workouts.aspx");
                }
            }
        }
    }
}