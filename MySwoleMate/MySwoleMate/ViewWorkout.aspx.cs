﻿using MySwoleMate.BLL;
using MySwoleMate.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MySwoleMate
{
    public partial class ViewWorkout : System.Web.UI.Page
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["MySwoleMateConnectionString"].ToString();

        WorkoutBLL workoutBLL = new WorkoutBLL(connectionString);
        TraineeBLL traineeBLL = new TraineeBLL(connectionString);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Get Connection
            if (!IsPostBack)
            {
                //Bind Name
                var trainee = traineeBLL.GetTraineeById(Convert.ToInt32(Request.QueryString["TraineeID"]));

                var workout = workoutBLL.GetWorkoutById(trainee.WorkoutID);

                lblTraineeName.Text = string.Format("{0} {1}", trainee.FirstName, trainee.LastName);
                lblWorkoutName.Text = string.Format("{0}", workout.Name);
                lblWorkout1.Text = string.Format("{0}", workout.DisplayExercise1);
                lblWorkout2.Text = string.Format("{0}", workout.DisplayExercise2);
                lblWorkout3.Text = string.Format("{0}", workout.DisplayExercise3);
                lblWorkout4.Text = string.Format("{0}", workout.DisplayExercise4);
                lblWorkout5.Text = string.Format("{0}", workout.DisplayExercise5);

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAssignWorkout_Click(object sender, EventArgs e)
        {
            string traineeID = Request.QueryString["TraineeID"].ToString();

            if (!string.IsNullOrWhiteSpace(traineeID))
            {
                Response.Redirect(string.Format("~/AssignWorkout.aspx?TraineeID={0}", traineeID));
            }
            else
            {
                Response.Redirect(string.Format("~/Trainees.aspx"));
            }
        }
    }
}