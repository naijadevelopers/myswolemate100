﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MySwoleMate.Master" AutoEventWireup="true" CodeBehind="AssignWorkout.aspx.cs" Inherits="MySwoleMate.AssignWorkout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="first">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1>Assign 5-Step Workout</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h3 id="ContentPlaceHolder1_FullName">
                        <asp:Label ID="TraineeLabel" runat="server" Text="{No Trainee}"></asp:Label>
                    </h3>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-8 col-xs-offset-2 text-center">
                    &nbsp;
                            <asp:DropDownList ID="WorkoutDropList" runat="server" Height="51px" Width="720px" CssClass="form-control">
                            </asp:DropDownList>
                    <br />
                    <div class="has-error">
                        <span class="help-block">
                            <asp:RequiredFieldValidator ID="WorkoutRequiredFieldValidator" runat="server" ErrorMessage="Please select another option other than --select workout--" ControlToValidate="WorkoutDropList" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-8 col-xs-offset-2 text-center">
                    <asp:Button ID="AssignBtn" runat="server" Text="Assign Workout" CssClass="btn btn-success" OnClick="AssignBtn_Click" />
                    &nbsp;<a href="Trainees.aspx" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
