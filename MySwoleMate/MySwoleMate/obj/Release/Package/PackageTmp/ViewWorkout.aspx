﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MySwoleMate.Master" AutoEventWireup="true" CodeBehind="ViewWorkout.aspx.cs" Inherits="MySwoleMate.ViewWorkout" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="first">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1>5-Step Workout</h1>
                    <h1 id="ContentPlaceHolder1_NoWorkout" class="text-center">
                        <asp:Label ID="lblTraineeName" runat="server" Text="Label"></asp:Label>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 id="ContentPlaceHolder1_WorkoutName">
                        <asp:Label ID="lblWorkoutName" runat="server" Text="Label"></asp:Label>
                    </h2>
                    <h3 id="ContentPlaceHolder1_DisplayExercise1">
                        <asp:Label ID="lblWorkout1" runat="server" Text="Label"></asp:Label>
                    </h3>
                    <h3 id="ContentPlaceHolder1_DisplayExercise2">
                        <asp:Label ID="lblWorkout2" runat="server" Text="Label"></asp:Label>
                    </h3>
                    <h3 id="ContentPlaceHolder1_DisplayExercise3">
                        <asp:Label ID="lblWorkout3" runat="server" Text="Label"></asp:Label>
                    </h3>
                    <h3 id="ContentPlaceHolder1_DisplayExercise4">
                        <asp:Label ID="lblWorkout4" runat="server" Text="Label"></asp:Label>
                    </h3>
                    <h3 id="ContentPlaceHolder1_DisplayExercise5">
                        <asp:Label ID="lblWorkout5" runat="server" Text="Label"></asp:Label>
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <asp:Button ID="btnAssignWorkout" runat="server" Text="Assign Workout" class="btn btn-success" OnClick="btnAssignWorkout_Click" />
&nbsp;<a href="Trainees.aspx" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
