﻿using MySwoleMate.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySwoleMate.DAL
{
    public class WorkoutDAL
    {
        private string _connectionString;

        public WorkoutDAL(string connectionString)
        {
            this._connectionString = connectionString;
        }


        //Create
        /// <summary>
        /// 
        /// </summary>
        /// <param name="workout"></param>
        /// <returns></returns>
        public int AddWorkoutItem(WorkoutViewModel workout)
        {
            string sqlQuery = "INSERT INTO Workout (Exercise1, Exercise1Reps, Exercise1Sets, " +
    "Exercise2, Exercise2Reps, Exercise2Sets, Exercise3, Exercise3Reps, Exercise3Sets, Exercise4, Exercise4Reps, Exercise4Sets," +
    "Exercise5, Exercise5Reps, Exercise5Sets, Name) VALUES (@Exercise1, @Exercise1Reps, @Exercise1Sets, " +
    "@Exercise2, @Exercise2Reps, @Exercise2Sets, @Exercise3, @Exercise3Reps, @Exercise3Sets, @Exercise4, @Exercise4Reps, @Exercise4Sets," +
    "@Exercise5, @Exercise5Reps, @Exercise5Sets, @Name)";
    

            //No need to use SqlDataReader here since we are just using the Sql Query to persist to database
            using (SqlConnection con = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
            {
                con.Open();
                //cmd.Parameters.Add("@WorkoutID", SqlDbType.Int).Value = workout.WorkoutID;
                cmd.Parameters.Add("@Exercise1", SqlDbType.VarChar).Value = workout.Excercise1;
                cmd.Parameters.Add("@Exercise1Reps", SqlDbType.Int).Value = workout.Excercise1Reps;
                cmd.Parameters.Add("@Exercise1Sets", SqlDbType.Int).Value = workout.Excercise1Sets;
                cmd.Parameters.Add("@Exercise2", SqlDbType.VarChar).Value = workout.Excercise2;
                cmd.Parameters.Add("@Exercise2Reps", SqlDbType.Int).Value = workout.Excercise2Reps;
                cmd.Parameters.Add("@Exercise2Sets", SqlDbType.Int).Value = workout.Excercise2Sets;
                cmd.Parameters.Add("@Exercise3", SqlDbType.VarChar).Value = workout.Excercise3;
                cmd.Parameters.Add("@Exercise3Reps", SqlDbType.Int).Value = workout.Excercise3Reps;
                cmd.Parameters.Add("@Exercise3Sets", SqlDbType.Int).Value = workout.Excercise3Sets;
                cmd.Parameters.Add("@Exercise4", SqlDbType.VarChar).Value = workout.Excercise4;
                cmd.Parameters.Add("@Exercise4Reps", SqlDbType.Int).Value = workout.Excercise4Reps;
                cmd.Parameters.Add("@Exercise4Sets", SqlDbType.Int).Value = workout.Excercise4Sets;
                cmd.Parameters.Add("@Exercise5", SqlDbType.VarChar).Value = workout.Excercise5;
                cmd.Parameters.Add("@Exercise5Reps", SqlDbType.Int).Value = workout.Excercise5Reps;
                cmd.Parameters.Add("@Exercise5Sets", SqlDbType.Int).Value = workout.Excercise5Sets;
                cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = workout.Name;

                int x = cmd.ExecuteNonQuery();

                return x;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WorkoutViewModel GetWorkOutById(int id)
        {
            WorkoutViewModel workout = new WorkoutViewModel();
            string sqlQuery = "SELECT * FROM Workout WHERE WorkoutID=@WorkoutID";

            using (SqlConnection con = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
            {
                con.Open();
                cmd.Parameters.Add("@WorkoutID", SqlDbType.Int).Value = id;
                //SqlDataReader is used because we are reading data from the database
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    //while there are records in the database
                    while (reader.Read())
                    {
                        //store each value into the properties of WorkoutViewModel

                        workout.WorkoutID = Convert.ToInt32(reader["WorkoutID"]);
                        workout.Name = reader["Name"].ToString();
                        workout.Excercise1 = reader["Exercise1"].ToString();
                        workout.Excercise1Reps = Convert.ToInt32(reader["Exercise1Reps"]);
                        workout.Excercise1Sets = Convert.ToInt32(reader["Exercise1Sets"]);
                        workout.Excercise2 = reader["Exercise2"].ToString();
                        workout.Excercise2Reps = Convert.ToInt32(reader["Exercise2Reps"]);
                        workout.Excercise2Sets = Convert.ToInt32(reader["Exercise2Sets"]);
                        workout.Excercise3 = reader["Exercise3"].ToString();
                        workout.Excercise3Reps = Convert.ToInt32(reader["Exercise3Reps"]);
                        workout.Excercise3Sets = Convert.ToInt32(reader["Exercise3Sets"]);
                        workout.Excercise4 = reader["Exercise4"].ToString();
                        workout.Excercise4Reps = Convert.ToInt32(reader["Exercise4Reps"]);
                        workout.Excercise4Sets = Convert.ToInt32(reader["Exercise4Sets"]);
                        workout.Excercise5 = reader["Exercise5"].ToString();
                        workout.Excercise5Reps = Convert.ToInt32(reader["Exercise5Reps"]);
                        workout.Excercise5Sets = Convert.ToInt32(reader["Exercise5Sets"]);

                    }
                }

            }

            return workout;
        }

        //Read
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<WorkoutViewModel> GetWorkoutList()
        {
            //SQL command for selecting all from Trainee table
            string sqlQuery = "SELECT * FROM Workout";

            //Empty list of WorkoutViewModel to add and return
            List<WorkoutViewModel> workoutList = new List<WorkoutViewModel>();

            //Using SqlConnection to establish connection to database
            //using SqlCommand passing in the SqlConnection and the sqlQuery
            using (SqlConnection con = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
            {
                //open the connection
                con.Open();
                //SqlDataReader is used because we are reading data from the database
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    //while there are records in the database
                    while (reader.Read())
                    {
                        //store each value into the properties of WorkoutViewModel
                        WorkoutViewModel temp = new WorkoutViewModel()
                        {
                            WorkoutID = Convert.ToInt32(reader["WorkoutID"]),
                            Name = reader["Name"].ToString(),
                            Excercise1 = reader["Exercise1"].ToString(),
                            Excercise1Reps = Convert.ToInt32(reader["Exercise1Reps"]),
                            Excercise1Sets = Convert.ToInt32(reader["Exercise1Sets"]),
                            Excercise2 = reader["Exercise2"].ToString(),
                            Excercise2Reps = Convert.ToInt32(reader["Exercise2Reps"]),
                            Excercise2Sets = Convert.ToInt32(reader["Exercise2Sets"]),
                            Excercise3 = reader["Exercise3"].ToString(),
                            Excercise3Reps = Convert.ToInt32(reader["Exercise3Reps"]),
                            Excercise3Sets = Convert.ToInt32(reader["Exercise3Sets"]),
                            Excercise4 = reader["Exercise4"].ToString(),
                            Excercise4Reps = Convert.ToInt32(reader["Exercise4Reps"]),
                            Excercise4Sets = Convert.ToInt32(reader["Exercise4Sets"]),
                            Excercise5 = reader["Exercise5"].ToString(),
                            Excercise5Reps = Convert.ToInt32(reader["Exercise5Reps"]),
                            Excercise5Sets = Convert.ToInt32(reader["Exercise5Sets"]),
                        };

                        //Add the Workout object to the List of Workout
                        workoutList.Add(temp);
                    }
                }
            }
            return workoutList;
        }

        //Update
        public int EditWorkOut(WorkoutViewModel workout)
        {
            string sqlQuery = "Update Workout Set Exercise1=@Exercise1, Exercise1Reps=@Exercise1Reps, " +
    "Exercise1Sets=@Exercise1Sets, Exercise2=@Exercise2, Exercise2Reps=@Exercise2Reps, Exercise2Sets=@Exercise2Sets, Exercise3=@Exercise3, " +
    "Exercise3Reps=@Exercise3Reps, Exercise3Sets=@Exercise3Sets, Exercise4=@Exercise4, Exercise4Reps=@Exercise4Reps, Exercise4Sets=@Exercise4Sets," +
            "Exercise5=@Exercise5, Exercise5Reps=@Exercise5Reps, Exercise5Sets=@Exercise5Sets, Name=@Name Where WorkoutID=@WorkoutID";

            //No need to use SqlDataReader here since we are just using the Sql Query to persist to database
            using (SqlConnection con = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
            {
                con.Open();
                cmd.Parameters.Add("@WorkoutID", SqlDbType.Int).Value = workout.WorkoutID;
                cmd.Parameters.Add("@Exercise1", SqlDbType.VarChar).Value = workout.Excercise1;
                cmd.Parameters.Add("@Exercise1Reps", SqlDbType.Int).Value = workout.Excercise1Reps;
                cmd.Parameters.Add("@Exercise1Sets", SqlDbType.Int).Value = workout.Excercise1Sets;
                cmd.Parameters.Add("@Exercise2", SqlDbType.VarChar).Value = workout.Excercise2;
                cmd.Parameters.Add("@Exercise2Reps", SqlDbType.Int).Value = workout.Excercise2Reps;
                cmd.Parameters.Add("@Exercise2Sets", SqlDbType.Int).Value = workout.Excercise2Sets;
                cmd.Parameters.Add("@Exercise3", SqlDbType.VarChar).Value = workout.Excercise3;
                cmd.Parameters.Add("@Exercise3Reps", SqlDbType.Int).Value = workout.Excercise3Reps;
                cmd.Parameters.Add("@Exercise3Sets", SqlDbType.Int).Value = workout.Excercise3Sets;
                cmd.Parameters.Add("@Exercise4", SqlDbType.VarChar).Value = workout.Excercise4;
                cmd.Parameters.Add("@Exercise4Reps", SqlDbType.Int).Value = workout.Excercise4Reps;
                cmd.Parameters.Add("@Exercise4Sets", SqlDbType.Int).Value = workout.Excercise4Sets;
                cmd.Parameters.Add("@Exercise5", SqlDbType.VarChar).Value = workout.Excercise5;
                cmd.Parameters.Add("@Exercise5Reps", SqlDbType.Int).Value = workout.Excercise5Reps;
                cmd.Parameters.Add("@Exercise5Sets", SqlDbType.Int).Value = workout.Excercise5Sets;
                cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = workout.Name;

                return cmd.ExecuteNonQuery();
            }
        }

        //Delete
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteWorkout(int id)
        {
            string sqlQuery = "DELETE from Workout Where WorkoutID=@WorkoutID";
            using (SqlConnection con = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
            {
                con.Open();
                cmd.Parameters.Add("@WorkoutID", SqlDbType.Int).Value = id;
                return cmd.ExecuteNonQuery();
            }
        }
    }
}
